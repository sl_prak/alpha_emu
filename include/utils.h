#ifndef ALPHA_EMU_UTILS_H
#define ALPHA_EMU_UTILS_H

#include <bitset>
#include <cstddef>
#include <iostream>
#include <chrono>


// x^8 + x^2 + x^1 + x^0
std::byte crc8_calc(std::byte* first_byte, unsigned bytes_count) {
    std::byte xor_value;
    std::byte crc8{0xff};
    std::byte zero{0};

    for (unsigned idx = 0; idx < bytes_count; idx++) {
        std::byte byte = *(first_byte + idx);
        crc8 ^= byte;

        for (unsigned bit = 0; bit < 8; bit++) {
            xor_value = ((crc8 & std::byte{0x80}) != zero) ? std::byte{0x07} : std::byte{0x00};
            crc8 = ((crc8 << 1) & std::byte{0x00FF}) ^ xor_value;
        }
    }
    return crc8;
}


/*         
// print std::byte in bin 
std::ostream& operator<<(std::ostream& os, std::byte b){
    return os << std::bitset<8>(std::to_integer<int>(b));
}
*/


// print byte in hex 
std::ostream& operator<<(std::ostream& os, std::byte b){
    return os << "0x" << std::uppercase << std::hex << std::to_integer<int>(b) << std::dec ;
}


void printRaw(std::array<std::byte, 15>& asp_msg){
    for (auto byte: asp_msg)
    {
        std::cout << byte << "\t";
    }
    std::cout << std::endl;
}

#endif //ALPHA_EMU_UTILS_H
