#include "alpha_emu.h"
#include "utils.h"

#include <iostream>
#include <cstddef>
#include <chrono>
#include <thread>
#include <mutex>
#include <unordered_map>
#include <memory>

#include "rclcpp/rclcpp.hpp"

#include <fcntl.h> 
#include <errno.h>
#include <termios.h> 
#include <unistd.h> 
#include <signal.h>


// Incomment this to show msgs
//#define PRINT_INPUT_RAW
//#define PRINT_OUTPUT_RAW
#define DEBUG_INFO


using asp_msg_t = std::array<std::byte, 15>;  // asp == Alpha Serial Protocol (15 bytes)
using alpha_emu_method = void(AlphaEmuSerial::*)();

struct Timer;

extern std::unordered_map<uint16_t, alpha_emu_method> CAN_ID_MAP;
extern std::unordered_map<uint16_t, std::pair<Timer, alpha_emu_method>> REPLY_MAP;

std::string SERIAL_PORT;
std::string LOGGER_NAME = "LOG";

std::byte const START_BYTE{0xAA};
std::byte const REPLY_CAN_CH{0x2};

unsigned DRIVE_FREQ      = 70; // Hz
unsigned START_STOP_FREQ = 2;  // Hz

std::atomic_bool stop_thread = false;


// For simplified using Alpha serial protocol
struct Protocol{
    std::byte                start_byte;
    std::array<std::byte, 2> time;
    std::byte                can_ch;
    std::array<std::byte, 2> can_id;
    std::byte                can_cnc;
    std::byte                can_type;
    std::array<std::byte, 4> can_data;
    std::byte                can_state;
    std::byte                can_crc;
    std::byte                serial_crc;
};


// Reply timer
struct Timer{
    Timer(): Timer(1) {}

    Timer(unsigned freq): 
        freq_(std::chrono::milliseconds(1000 / freq)),
        last_update_(std::chrono::steady_clock::now())
    {}

public:
    // is timer ready to exec
    bool is_need_to_reply(){
        auto now = std::chrono::steady_clock::now();

        std::chrono::duration<double> difference = now - last_update_;

        if  (difference > freq_){
            last_update_ = now;  // reset timer
            return true;
        }
        return false;
    }

private:
    // Last upd time point 
    std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<long, std::ratio<1, 1'000'000'000>>> last_update_;

    // Frequency of reply publication 
    std::chrono::milliseconds freq_;
};


// Serial communication
class AlphaEmuSerial{
public:
    AlphaEmuSerial() = delete;
    
    AlphaEmuSerial(std::string serial_port);

    ~AlphaEmuSerial();

public:
    // Get data from serial and set protocol_ //
    void readSpin();

    bool connect();  

    bool setSerialConfig() const;

    bool processBuffer(std::array<std::byte, 1024>& read_buf, int& buffer_filled_size);

    bool parseMsg(asp_msg_t& asp_msg);

    void alphaMsgCallback();

    // From protocol_ set alpha_emu_ fields // 
    // from serial methods //

    void can_id_0x051();  

    void can_id_0x035();  

    void can_id_0x032();  

public:
    // Create reply from alpha_emu_ fields and send to serial //
    void writeSpin();

    void createAspTemplate(asp_msg_t& asp_msg);

    // to setial methonds //

    void can_id_0x001();  

    void can_id_0x003();  

    void can_id_0x015();  

public: 
    // Main object ptr with data
    AlphaEmu* alpha_emu_; 

    // Executable threads
    std::thread read_thread_;
    std::thread write_thread_;

    // Current readable msg data
    Protocol protocol_;

    // Serial file descriptor
    int file_descriptor_ = -1;
};


AlphaEmu::AlphaEmu(std::string serial_port):impl_(std::make_shared<AlphaEmuSerial>(serial_port)) { 
    impl_->alpha_emu_ = this;
}


std::unordered_map<uint16_t, alpha_emu_method> CAN_ID_MAP = {
    // {can_id, can_id_callback} // 
    {std::to_integer<uint16_t>(std::byte{0x32}), &AlphaEmuSerial::can_id_0x032},
    {std::to_integer<uint16_t>(std::byte{0x35}), &AlphaEmuSerial::can_id_0x035},
    {std::to_integer<uint16_t>(std::byte{0x51}), &AlphaEmuSerial::can_id_0x051}

};

std::unordered_map<uint16_t, std::pair<Timer, alpha_emu_method>> REPLY_MAP{
    // {can_id, pair<Timer(freq), can_id_callback>} // 
    {1,  std::make_pair(Timer(DRIVE_FREQ),      &AlphaEmuSerial::can_id_0x001)},
    {3,  std::make_pair(Timer(DRIVE_FREQ),      &AlphaEmuSerial::can_id_0x003)},
    {15, std::make_pair(Timer(START_STOP_FREQ), &AlphaEmuSerial::can_id_0x015)}

};


AlphaEmuSerial::AlphaEmuSerial(std::string serial_port){
    SERIAL_PORT = serial_port;
    if (connect()){
        read_thread_  = std::thread ([this]() { readSpin(); });
        write_thread_ = std::thread ([this]() { writeSpin(); });
    }
}


AlphaEmuSerial::~AlphaEmuSerial(){
    if (file_descriptor_ >= 0) {
        // stop threads
        stop_thread = true;

        read_thread_.join();
        write_thread_.join();

        stop_thread = false;

        // close serial port
        close(file_descriptor_);

        file_descriptor_ = -1;
    }
}


void AlphaEmuSerial::writeSpin() {
    while (!stop_thread)
    {
        for (auto& [can_id, timer_method_pair] : REPLY_MAP){
            if (timer_method_pair.first.is_need_to_reply()){
                (this->*timer_method_pair.second)();  // exec method
            }
        }
    }
}


void AlphaEmuSerial::readSpin() {
    std::array<std::byte, 1024> read_buf;
    int buffer_filled_size = 0;

    RCLCPP_INFO(rclcpp::get_logger(LOGGER_NAME.c_str()), "Conneced to %s.", SERIAL_PORT.c_str());

    while (!stop_thread)
    {
        int read_bytes_count = read(file_descriptor_,   read_buf.data() + buffer_filled_size, read_buf.size() - buffer_filled_size);

        buffer_filled_size += read_bytes_count;

        if (read_bytes_count < 0) {
            RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), "%s", strerror(errno));  
            return;
        }  

        if (!processBuffer(read_buf, buffer_filled_size)){
            // entire filled buffer is garbage: clear buffer

            buffer_filled_size = 0;
        }
    }
}


bool AlphaEmuSerial::processBuffer(std::array<std::byte, 1024>& read_buf, int& buffer_filled_size){
    if (buffer_filled_size < 15) { return true; }

    asp_msg_t tmp_buffer;
    
    bool successful_process = false;  // false if filled buffer does not contain a START_BYTE

    for (int idx = 0; idx < buffer_filled_size; idx++)
    {
        if (read_buf[idx] == START_BYTE){
            if (idx > buffer_filled_size - 15) { 
                return successful_process; 
            }

            // copy from read buffer to tmp buffer: get one asp msg
            std::memcpy(&tmp_buffer[0], &read_buf[idx], 15);  // sizeof(Alpha Serial Protocol) == 15 bytes

            if (parseMsg(tmp_buffer)) { 
                alphaMsgCallback();
            } else {
                continue;
            }

            // shift read buffer and his size to a new message
            std::memcpy(&read_buf[0], &read_buf[idx + 15], buffer_filled_size - idx - 15);
            buffer_filled_size -= idx + 15;

            successful_process = true;

            //if (buffer_filled_size < 15) { break;}
        }
    }
    return successful_process;
}


bool AlphaEmuSerial::connect() {
    if (SERIAL_PORT.empty()) {
        RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), "Connection failed. Empty serial port name.");  
        return false;
    }

    file_descriptor_ = open(SERIAL_PORT.c_str(), O_RDWR);

    if (file_descriptor_ < 0) {
        RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()),
            "Connecting to %s failed. Error from open: %s", 
            SERIAL_PORT.c_str(),
            strerror(errno));
        return false;
    }

    return setSerialConfig();
}


bool AlphaEmuSerial::setSerialConfig() const {
    struct termios tty;

    if(tcgetattr(file_descriptor_, &tty) != 0) {
        RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), 
            "Error code %d from tcgetattr: %s", errno, strerror(errno));
        return false;
    }

    tty.c_cflag &= ~PARENB;         
    tty.c_cflag &= ~CSTOPB;         
    tty.c_cflag &= ~CSIZE;         
    tty.c_cflag |= CS8;             
    tty.c_cflag &= ~CRTSCTS;        
    tty.c_cflag |= CREAD | CLOCAL;  

    tty.c_lflag &= ~ICANON;         
    tty.c_lflag &= ~ECHO;           
    tty.c_lflag &= ~ECHOE;          
    tty.c_lflag &= ~ECHONL;         
    tty.c_lflag &= ~ISIG;           

    tty.c_iflag &= ~(IXON | IXOFF | IXANY);
    tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);

    tty.c_oflag &= ~OPOST;
    tty.c_oflag &= ~ONLCR;

    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
    tty.c_cc[VTIME] = 10;    
    tty.c_cc[VMIN] = 0;

    // Set in/out baud rate to be 115200
    cfsetispeed(&tty, B115200);
    cfsetospeed(&tty, B115200);

    if (tcsetattr(file_descriptor_, TCSANOW, &tty) != 0) {
        RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), 
            "Error %d from tcgetattr: %s", errno, strerror(errno));
        return false;
    }
    return true;
}


void AlphaEmuSerial::can_id_0x001(){  
    asp_msg_t reply;
    createAspTemplate(reply);

    reply[4]  = std::byte{0x3};  // can id
    reply[5]  = std::byte{0x0};  // can id

    reply[7]  = std::byte{0x4};  // can type

    reply[8]  = static_cast<std::byte>(alpha_emu_->vehicle_speed_ & 0xFF);  // SPEED_L 
    reply[9]  = static_cast<std::byte>(alpha_emu_->vehicle_speed_ >> 8);    // SPEED_H 
    reply[10] = static_cast<std::byte>(alpha_emu_->vehicle_acceleration_ & 0xFF);  // ACCELERATION_L
    reply[11] = static_cast<std::byte>(alpha_emu_->vehicle_acceleration_ >> 8);    // ACCELERATION_H

    reply[13] = crc8_calc(&reply[0], 14);
    reply[14] = crc8_calc(&reply[6], 7);

#ifdef PRINT_OUTPUT_RAW
    printRaw(reply);
#endif

    write(file_descriptor_, reply.data(), reply.size());
}


void AlphaEmuSerial::can_id_0x003(){
    asp_msg_t reply;
    createAspTemplate(reply);

    reply[4]  = std::byte{0x1};  // can id 
    reply[5]  = std::byte{0x0};  // can id 

    reply[7]  = std::byte{0x4};  // can type

    reply[8]  = static_cast<std::byte>(alpha_emu_->steering_angle_ & 0xFF);  // ANGLE_L 
    reply[9]  = static_cast<std::byte>(alpha_emu_->steering_angle_ >> 8);    // ANGLE_H 
    reply[10] = static_cast<std::byte>(alpha_emu_->steering_velocity_ & 0xFF);  // VELOCITY_L
    reply[11] = static_cast<std::byte>(alpha_emu_->steering_velocity_ >> 8);    // VELOCITY_H

    reply[13] = crc8_calc(&reply[0], 14);
    reply[14] = crc8_calc(&reply[6], 7);

#ifdef PRINT_OUTPUT_RAW
    printRaw(reply);
#endif

    write(file_descriptor_, reply.data(), reply.size());
}


void AlphaEmuSerial::can_id_0x015(){
    asp_msg_t reply;
    createAspTemplate(reply);

    reply[4]  = std::byte{0x15};  // can id
    reply[5]  = std::byte{0x0};   // can id

    reply[7]  = std::byte{0x1};  // can type

    // TAKEOVER_BUTTON 
    switch (alpha_emu_->mode_plugin_)
    {
    case DRIVE:
        reply[8]  = std::byte{0b01101010};   
        break;
    case REVERSE:
        reply[8]  = std::byte{0b01101011};   
        break;    
    case MANUAL:
        reply[8]  = std::byte{0b01101100};   
        break;    
    default:
        reply[8]  = std::byte{0b01101000};   
        break;
    }

    // LED
    reply[9]  = static_cast<std::byte>(alpha_emu_->led_state_plugin_);        

    // EMER_BRAKE
    reply[10] = (alpha_emu_->emer_brake_state_plugin_) ? std::byte{0b01101001} : std::byte{0b01101010};

    // HAND_BRAKE
    reply[11] = (alpha_emu_->hand_brake_state_plugin_) ? std::byte{0b01101001} : std::byte{0b01101010}; 

    reply[13] = crc8_calc(&reply[0], 14);
    reply[14] = crc8_calc(&reply[6], 7);

#ifdef PRINT_OUTPUT_RAW
    printRaw(reply);
#endif

    write(file_descriptor_, reply.data(), reply.size());
}


void AlphaEmuSerial::createAspTemplate(asp_msg_t& asp_msg){
    asp_msg[0]  = START_BYTE;
    asp_msg[1]  = std::byte{0x0};  // time
    asp_msg[2]  = std::byte{0x0};  // time
    asp_msg[3]  = REPLY_CAN_CH;  

    asp_msg[6]  = std::byte{0xFF};  // can cnc
    asp_msg[12] = std::byte{0x1};   // state


    // set other bytes to zeros
    asp_msg[4]  = std::byte{0x0};  // can id
    asp_msg[5]  = std::byte{0x0};  // can id

    asp_msg[7]  = std::byte{0x0};  // can type
    asp_msg[8]  = std::byte{0x0};  // can data 
    asp_msg[9]  = std::byte{0x0};  // can data 
    asp_msg[10] = std::byte{0x0};  // can data
    asp_msg[11] = std::byte{0x0};  // can data

    asp_msg[13] = std::byte{0x0};  // CRC
    asp_msg[14] = std::byte{0x0};  // CRC
}


void AlphaEmuSerial::alphaMsgCallback(){
    uint16_t can_id = (static_cast<uint16_t>(protocol_.can_id[1]) << 8) | static_cast<uint16_t>(protocol_.can_id[0]);
            
    // find can id and call his method
    auto exec_method  =  CAN_ID_MAP[can_id];

    if (exec_method == nullptr){
#ifdef DEBUG_INFO
        RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), "Invalid can id! Msg ignored.");
#endif
        return;
    } 

    (this->*exec_method)();

}


bool AlphaEmuSerial::parseMsg(asp_msg_t& asp_msg) {
    protocol_.start_byte  = asp_msg[0] ;
    protocol_.time[0]     = asp_msg[1] ;
    protocol_.time[1]     = asp_msg[2] ;
    protocol_.can_ch      = asp_msg[3] ;
    protocol_.can_id[0]   = asp_msg[4] ;
    protocol_.can_id[1]   = asp_msg[5] ;
    protocol_.can_cnc     = asp_msg[6] ;
    protocol_.can_type    = asp_msg[7] ;
    protocol_.can_data[0] = asp_msg[8] ;
    protocol_.can_data[1] = asp_msg[9] ;
    protocol_.can_data[2] = asp_msg[10];
    protocol_.can_data[3] = asp_msg[11];
    protocol_.can_state   = asp_msg[12];
    protocol_.can_crc     = asp_msg[13];
    protocol_.serial_crc  = asp_msg[14];

#ifdef PRINT_INPUT_RAW
    printRaw(asp_msg);
#endif

    // check serial crc: calculate for 14 bytes(one asp msg) starting from start byte[0]   
    if (crc8_calc(&asp_msg[0], 14) != asp_msg[14]){
#ifdef DEBUG_INFO
        RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), "Serial crc wrong! Msg ignored. ");
        printRaw(asp_msg);
#endif
    }

    // check can crc: calculate for 7 bytes(acp) starting from can counter
    else if (crc8_calc(&asp_msg[6], 7) != asp_msg[13]){
#ifdef DEBUG_INFO
        RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), "CAN crc wrong! Msg ignored.");
#endif
    }

    else if (protocol_.can_ch != std::byte{0x1} and protocol_.can_ch != std::byte{0x2}){
#ifdef DEBUG_INFO
        RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), "Invalid CAN channel! Msg ignored.");
#endif
    }

    else { return true; }

    return false;
}


void AlphaEmuSerial::can_id_0x051(){
    alpha_emu_->throttle_ = static_cast<int16_t>(protocol_.can_data[3]) << 8 | static_cast<int16_t>(protocol_.can_data[2]); 
}


void AlphaEmuSerial::can_id_0x035(){
    // TAKEOVER_BUTTON
    switch (protocol_.can_data[0]){
        case std::byte{0x0}:
            // Don't change the current state - pass
            break;

        case std::byte{0x2}:
            alpha_emu_->mode_serial_ = DRIVE;
            break;

        case std::byte{0x3}:
            alpha_emu_->mode_serial_ = REVERSE;
            break;

        case std::byte{0x5}:
            alpha_emu_->mode_serial_ = MANUAL;
            break;            

        default:
#ifdef DEBUG_INFO
            RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), "Invalid takeover button state!");
#endif      
            break;
    }

    // LED
    switch (protocol_.can_data[1]){
        case std::byte{0x0}:
            // Don't change the current state - pass
            break;

        case std::byte{0x1}:
#ifdef DEBUG_INFO
            RCLCPP_INFO(rclcpp::get_logger(LOGGER_NAME.c_str()), "BLIK!");
#endif        
            alpha_emu_->led_state_serial_ = true;
            break;

        case std::byte{0x2}:
            alpha_emu_->led_state_serial_ = false;
            break;

        default:
#ifdef DEBUG_INFO
            RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), "Invalid LED state!");
#endif  
            break;
    }
    
    // EMER_BRAKE
    switch (protocol_.can_data[2]){
        case std::byte{0x0}:
            // Don't change the current state - pass
            break;

        case std::byte{0x1}:
            alpha_emu_->emer_brake_state_serial_ = true;
            break;

        case std::byte{0x2}:
            alpha_emu_->emer_brake_state_serial_ = false;
            break;

        default:
#ifdef DEBUG_INFO
            RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), "Invalid emer brake state!");
#endif  
            break;
    }

    // HAND_BRAKE
    switch (protocol_.can_data[3]){
        case std::byte{0x0}:
            // Don't change the current state - pass
            break;

        case std::byte{0x1}:
            alpha_emu_->hand_brake_state_serial_ = true;
            break;

        case std::byte{0x2}:
            alpha_emu_->hand_brake_state_serial_ = false;
            break;

        default:
#ifdef DEBUG_INFO
            RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), "Invalid hand brake state!");
#endif  
            break;
    }
}


void AlphaEmuSerial::can_id_0x032(){
    int16_t torque = static_cast<int16_t>(protocol_.can_data[3]) << 8 | static_cast<int16_t>(protocol_.can_data[2]);

    switch (protocol_.can_data[1])
    {
    case std::byte{0x0}:
        // Don't change the current state - pass
        break;
    
    case std::byte{0x1}:
        alpha_emu_->steering_torque_ = - torque;  // clockwise according to the protocol
        break;

    case std::byte{0x2}:
        alpha_emu_->steering_torque_ = torque;   // counterclockwise according to the protocol
        break;

    default:
#ifdef DEBUG_INFO
        RCLCPP_ERROR(rclcpp::get_logger(LOGGER_NAME.c_str()), "Invalid steering direction!");
#endif        
        break;
 
    }
}